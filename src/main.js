import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

/*.. cherrypicking .. */
import { MdApp, MdList, MdIcon, MdTable, MdDrawer, MdToolbar, MdContent, MdCard, MdRipple, MdCheckbox, MdButton, MdField, MdMenu, MdDivider, MdSnackbar, MdDialog } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
/* import 'vue-material/dist/theme/default.css' */

Vue.use(MdApp)
Vue.use(MdList)
Vue.use(MdIcon)
Vue.use(MdTable)
Vue.use(MdDrawer)
Vue.use(MdToolbar)
Vue.use(MdContent)
Vue.use(MdCard)
Vue.use(MdRipple)
Vue.use(MdCheckbox)
Vue.use(MdButton)
Vue.use(MdField)
Vue.use(MdMenu)
Vue.use(MdDivider)
Vue.use(MdSnackbar)
Vue.use(MdDialog)

Vue.config.productionTip = false
export const bus = new Vue(); // Let's listen to events everywhere .. 

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
