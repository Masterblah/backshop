export function populator () {
   let database = JSON.parse(window.localStorage.getItem('database'));
   const products = [
    {
       title:"Turbant",
       price: "24",
       color:[
          "#A463BF",
          "#3D556E",
          "#F2C511"
       ],
       date:null,
       labels:[
          "turbant",
          "yellow",
          "cool"
       ],
       avatar:[
          4,
          1,
          3,
          7,
          0,
          2,
          5,
          4,
          13,
          4,
          6,
          5
       ],
       created:"11/29/2020, 12:38:10 AM",
       Uid:"D"+ Date.now().toString()+1,
       order:0
    },
    {
       title:"Amish",
       price:"23",
       color:[
          "#A463BF",
          "#222F3D"
       ],
       date:null,
       labels:[
          "worries",
          "amish",
          "punk"
       ],
       avatar:[
          2,
          0,
          0,
          9,
          9,
          1,
          8,
          1,
          6,
          0,
          3,
          3
       ],
       created:"11/29/2020, 12:38:48 AM",
       Uid:"C"+ Date.now().toString()+2,
       order:1
    },
    {
       title:"Happy Afro",
       price:"34",
       color:[
          "#F2C511",
          "#A463BF",
          "#C0382B",
          "#3398DB",
          "#BDC3C8"
       ],
       date:null,
       labels:[
          "cool",
          "afro",
          "style"
       ],
       avatar:[
          16,
          0,
          10,
          3,
          5,
          1,
          8,
          6,
          11,
          11,
          5,
          4
       ],
       created:"11/29/2020, 12:40:26 AM",
       Uid:"B"+ Date.now().toString()+3,
       order:2
    },
    {
       title:"Hipster",
       price:"23",
       color:[
          "#8E43AD",
          "#3398DB",
          "#222F3D",
          "#1FBC9C"
       ],
       date:null,
       labels:[
          "hipster",
          "cap",
          "mug"
       ],
       avatar:[
          6,
          0,
          1,
          3,
          8,
          1,
          5,
          8,
          6,
          11,
          0,
          4
       ],
       created:"11/29/2020, 12:41:48 AM",
       Uid:"A"+Date.now().toString()+4,
       order:3
    }
 ] ;
    if(database) {
        database[0].products.push(...products)
    } else {
        database = [{ products:[] }]
        database[0].products.push(...products)
    }
    window.localStorage.setItem('database', JSON.stringify(database))
    return database
}
