import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    createdDB: JSON.parse(window.localStorage.getItem('createdDB')),
    createdDBid: JSON.parse(window.localStorage.getItem('createdDBid')),
    crudcrud: JSON.parse(window.localStorage.getItem('crudcrud')),
    localDatabase: JSON.parse(window.localStorage.getItem('database')),
    database: JSON.parse(window.localStorage.getItem('database')),
    product: {
      placeholder: ''
    },
    loading: false,
  },
  actions: {
    createInitial({commit}, database) { commit('setStateDB', database) },
    
    async createDB( {commit}, crudcrud ){ // using api as a big DB
    commit('storeCrud', crudcrud)
    const products =  this.state.database[0]
     const response = await axios.post(crudcrud + '/products', products).catch( err => {
      console.log(err)
      commit('commitError')
     }
    )
    commit('storeDBId', response.data._id)
  },
  async getAllProducts({commit}) {
    const response = await axios.get(this.state.crudcrud + '/products')
    commit('storeDB', response.data)
  },
  async updateDB({commit}) {
    /* delete payload._id */
    await axios.put(this.state.crudcrud + '/products/' + this.state.createdDBid, this.state.database[0]).then( async getAll => {
      getAll = await axios.get(this.state.crudcrud + '/products')
      delete getAll.data[0]._id
      commit('setStateDB', getAll.data)
    },
    commit('commitSuccess', 'Success')
    ).catch( err => {
      console.log(err.request)
      console.log(err.response)
      commit('commitError')
      })
    }
  },
  mutations: {
    storeCrud: (state, crudcrud) => state.crudcrud = crudcrud,
    storeDBId: (state, createdDBid) => {
      window.localStorage.setItem('createdDBid', JSON.stringify(createdDBid));
      state.createdDBid = createdDBid
    },
    setStateDB: (state, database) => {
      console.log('updating...')
      state.database = database
      window.localStorage.setItem('database', JSON.stringify(database));
    },
    setProductsOrder: (state, products) => {
      state.database[0].products = products
      window.localStorage.setItem('database', JSON.stringify(state.database));
    },
    setSingleProduct: (state, product) => { state.product = product },
    commitSuccess: (state, status) => state.status = status,
    commitError: (state) => state.crudcrud = false
  }
})
