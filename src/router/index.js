import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'

Vue.use(VueRouter)

// Solves the click on same route issue caused by recent vue router issue //
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/',
    component: Dashboard,
    children: [
      {
        path: '/',
        name: 'Products',
        component: () => import(/* webpackChunkName: "Products" */ '../components/ProductList.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: "md-accent", // active class for non-exact links.
  linkExactActiveClass: "md-accent" // active class for *exact* links.
})

export default router
