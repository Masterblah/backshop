// vue.config.js
module.exports = {
  devServer: {
    port: 8080,
  },
    css: {
      loaderOptions: {
        scss: {
          additionalData: `@import "~@/assets/scss/theme.scss";`
        }
      }
    }
  }